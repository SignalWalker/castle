#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform Camera {
    mat4 cam;
} camera;

layout(location = 0) in vec3 v_pos;
layout(location = 1) in vec2 v_uv;
layout(location = 2) in vec3 v_norm;
layout(location = 3) in vec3 v_color;

layout(location = 0) out vec2 f_uv;
layout(location = 1) out vec3 f_norm;
layout(location = 2) out vec3 f_color;

void main() {
  f_uv = v_uv;
  gl_Position = camera.cam * vec4(v_pos, 1.0);
  f_norm = v_norm;
  f_color = v_color;
}