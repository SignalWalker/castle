use ash::vk;
use vaal::map::{render::TerrainRenderer, Map};

use flint::base::swapchain::SwapToken;
use vaal::flint;
use vaal::render::Camera;
use vaal::Simulator;

use flint::winit;
use flint::{base::VkData, sampler::*, texture::*, *};
use na::*;
use painter::lightcycle::na;
use vaal::legion::prelude::*;

use winit::{event::Event, event_loop::ControlFlow};

use std::time::Instant;

pub mod mason;
use mason::*;

pub struct Sim {
    cam_speed: f32,
    cam_rot: f32,
    init_cam_pos: Point3<f32>,
    camera: Camera,
    universe: Universe,
    map: Option<Map>,
    t_renderer: Option<TerrainRenderer>,
    tex: Option<Texture>,
    sampler: Option<SamplerToken>,
    settings: MetaMason,
}

impl Sim {
    pub fn new(window: &winit::window::Window, settings: MetaMason) -> Self {
        let init_cam_pos = [0.0, 0.0, 0.0].into();
        // let win_size = window.inner_size().to_physical(window.hidpi_factor());
        // let mut camera = Camera::with_ortho(
        //     init_cam_pos,
        //     Orthographic3::new(-12.0, 12.0, -12.0, 12.0, 0.0, 36.0),
        // );
        let mut camera = Camera::with_persp(
            init_cam_pos,
            Perspective3::new(
                {
                    let size = window.inner_size().to_physical(window.hidpi_factor());
                    (size.width / size.height) as f32
                },
                std::f32::consts::PI / 4.0,
                0.1,
                100.0,
            ),
        );
        camera.up = -camera.up;
        Sim {
            cam_speed: 0.01,
            cam_rot: std::f32::consts::PI * 0.0005,
            init_cam_pos,
            camera,
            universe: Universe::new(None),
            map: None,
            t_renderer: None,
            tex: None,
            sampler: None,
            settings,
        }
    }
}

impl Simulator for Sim {
    const NAME: &'static str = "Castle";
    const CLEAR_COLOR: [f32; 4] = [0.0, 0.0, 0.0, 0.8];

    fn icon() -> Option<winit::window::Icon> {
        let image = match image::open("data/gfx/tex/icon.png") {
            Ok(o) => o,
            Err(_) => return None,
        }
        .to_rgba();
        let dims = image.dimensions();
        let data = image.into_raw();
        match winit::window::Icon::from_rgba(data, dims.0, dims.1) {
            Ok(o) => Some(o),
            Err(_e) => None,
        }
    }

    fn init(&mut self, vk: &mut VkData, swapchain: &mut SwapToken) {
        use crate::map::*;
        use flint::shader::gen_material;
        use vaal::map::*;

        self.map = Some(Map::new(
            self.universe.create_world(),
            Box::new(RogueCartographer::default()),
        ));
        self.map.as_mut().unwrap().load_chunk(&Point3::origin());

        let tex_time = Instant::now();
        self.tex = Some(Texture::from_path(
            vk.device.clone(),
            &vk.device_memory_properties,
            swapchain.cmd_pool.buffers[0],
            vk.present_queue,
            "data/gfx/tex/curses.png",
        ));
        println!("Texture Load Time: {:?}", tex_time.elapsed());

        self.sampler = Some(SamplerToken::new(vk.device.clone()));

        // Pipeline
        let tile_mat = gen_material(
            vk,
            swapchain,
            "std".to_string(),
            vec![
                ("data/gfx/shd/terrain.vert", "main"),
                ("data/gfx/shd/simple.frag", "main"),
            ],
            vk::PolygonMode::FILL,
        );

        {
            use flint::shader::DescWriteInfo::*;
            swapchain.pipes["std"].desc_pool.update_desc_sets(vec![(
                "samplerColor",
                Img(self
                    .tex
                    .as_mut()
                    .unwrap()
                    .tex_info(self.sampler.as_ref().unwrap().sampler)),
            )]);
        }

        self.t_renderer = Some(TerrainRenderer::default());
        self.t_renderer.as_mut().unwrap().gen_terrain_meshes(
            vk.device.clone(),
            &vk.device_memory_properties,
            &self.map.as_ref().unwrap().world,
            vec![("std".to_string(), tile_mat)],
        );
    }

    fn respond_to_event(&mut self, event: Event<()>, ctrl_flow: &mut ControlFlow) {
        use winit::event::WindowEvent;
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input,
                        device_id: _device_id,
                    },
                ..
            } => {
                #[cfg(debug)]
                println!("State: {:?}, ScanCode: {}", input.state, input.scancode);
                for action in self.settings.settings.control.handle_key(input) {
                    #[cfg(debug)]
                    println!("Action: {}", action);
                    match &action[..] {
                        "camera" => {
                            self.camera = Camera::new(self.init_cam_pos, self.camera.proj);
                        }
                        "quit" => *ctrl_flow = ControlFlow::Exit,
                        _ => (),
                    }
                }
            }
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => *ctrl_flow = ControlFlow::Exit,
            Event::DeviceEvent { .. } => (),
            _ => (),
        }
    }

    fn simulate(&mut self, ticks: f32, _ctrl_flow: &mut ControlFlow) {
        let cam_speed = self.cam_speed * ticks;
        let cam_rot = self.cam_rot * ticks;
        for action in &self.settings.settings.control.active {
            #[cfg(debug_assertions)]
            println!("Action: {}", action);
            match &action[..] {
                "camera_up" => self.camera.mov(0.0, cam_speed, 0.0),
                "camera_down" => self.camera.mov(0.0, -cam_speed, 0.0),
                "camera_left" => self.camera.mov(-cam_speed, 0.0, 0.0),
                "camera_right" => self.camera.mov(cam_speed, 0.0, 0.0),
                "camera_in" => self.camera.mov(0.0, 0.0, cam_speed),
                "camera_out" => self.camera.mov(0.0, 0.0, -cam_speed),
                "camera_rot_left" => self.camera.rot(&(self.camera.up * -cam_rot)),
                "camera_rot_right" => self.camera.rot(&(self.camera.up * cam_rot)),
                "camera_rot_fore" => self.camera.rot(&(self.camera.right() * -cam_rot)),
                "camera_rot_aft" => self.camera.rot(&(self.camera.right() * cam_rot)),
                "camera_spin_left" => self.camera.rot(&(self.camera.forward * cam_rot)),
                "camera_spin_right" => self.camera.rot(&(self.camera.forward * -cam_rot)),
                _ => (),
            }
        }
    }

    fn init_render(&mut self, vk: &VkData, swapchain: &mut SwapToken) {
        // dbg!(&self.camera);
        for (_k, pipe) in swapchain.pipes.iter_mut() {
            pipe.desc_pool.init_buffers(&vk.device_memory_properties);
            pipe.desc_pool
                .write_buffer("Camera", "cam", self.camera.fresh_mat().as_slice());
            pipe.desc_pool.refresh_desc_sets();
        }
    }

    fn render(&mut self, swapchain: &SwapToken, _dev: &ash::Device, cmd_buffer: vk::CommandBuffer) {
        self.t_renderer
            .as_ref()
            .unwrap()
            .render(cmd_buffer, swapchain);
    }

    fn resized_swapchain(&mut self, size: (f64, f64)) {
        self.camera.resize(size.0 as _, size.1 as _);
    }
}
