use na::Point3;
use vaal::render::Material;

use vaal::flint::lightcycle::na;
use vaal::legion::prelude::*;
use vaal::map::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Default)]
pub struct RogueCartographer;
impl Cartographer for RogueCartographer {
    fn load_chunk(&mut self, chunk: &Point3<i32>, world: &mut World) {
        use Side::*;
        let mut tiles = Vec::new();
        for (pos, side) in vec![
            (0, Bottom),
            (11, Top),
            (11, Front),
            (0, Back),
            (11, Left),
            (0, Right),
        ]
        .into_iter()
        {
            for x in 0..12 {
                for y in 0..12 {
                    let pos = match side {
                        Top | Bottom => Point3::new(x, pos, y),
                        Front | Back => Point3::new(x, y, pos),
                        Left | Right => Point3::new(pos, x, y),
                        // _ => unreachable!(),
                    };
                    tiles.push((
                        Tile {
                            side,
                            img_index: 43,
                            shape: 1,
                            rot: Rotation::Fore,
                            dir: Direction::In,
                            color: [
                                u8::max(1, (pos.x % 256) as u8),
                                u8::max(1, (pos.y % 256) as u8),
                                u8::max(1, (pos.z % 256) as u8),
                            ],
                            // color: match side {
                            //     Top => [64, 32, 32],
                            //     Bottom => [128, 64, 32],
                            //     Front => [32, 128, 64],
                            //     Back => [32, 64, 32],
                            //     Left => [64, 32, 128],
                            //     Right => [32, 32, 64],
                            //     // _ => unreachable!(),
                            // },
                        },
                        GridPos(pos),
                    ));
                }
            }
        }
        let shared = (Material(0), ChunkPos(*chunk), Static);
        world.insert_from(shared, tiles);
    }
}

pub struct GridMap {}
