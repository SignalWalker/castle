# Castle

The first in a series of games, each made in one week using [flint](https://gitlab.com/SignalWalker/flint) and [vaal](https://gitlab.com/SignalWalker/vaal).

This one is a tiny 3D roguelike.

## Compile & Run

### Requirements

* Vulkan SDK
* [Rust](https://rust-lang.org)

### Compile

```
git clone https://gilab.com/SignalWalker/castle
cd castle
cargo run --release
```

## Controls

* Translate Camera
    * X: A, D
    * Y: Q, E
    * Z: W, S
* Rotate Camera
    * X: Numpad 8, 2
    * Y: Numpad 4, 6
    * Z: Numpad 7, 9
* Reset Camera Position: C
* Quit: Escape

## Settings

Settings can be changed in [meta.stn](data/meta.stn).

## TODO

*